class Book < ActiveRecord::Base
  attr_accessible :author, :description, :mirror_links, :publisher, :source, :title, :year, :upload1, :upload2, :license

  has_attached_file :upload1
  has_attached_file :upload2

  after_initialize do
    self.license = "This work is in the public domain in India because its term of copyright has expired." unless self.license
  end
end
