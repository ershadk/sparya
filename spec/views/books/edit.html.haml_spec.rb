require 'spec_helper'

describe "books/edit" do
  before(:each) do
    @book = assign(:book, stub_model(Book,
      :title => "MyText",
      :author => "MyText",
      :year => "MyString",
      :publisher => "MyText",
      :description => "MyText",
      :source => "MyText",
      :mirror_links => "MyText"
    ))
  end

  it "renders the edit book form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", book_path(@book), "post" do
      assert_select "textarea#book_title[name=?]", "book[title]"
      assert_select "textarea#book_author[name=?]", "book[author]"
      assert_select "input#book_year[name=?]", "book[year]"
      assert_select "textarea#book_publisher[name=?]", "book[publisher]"
      assert_select "textarea#book_description[name=?]", "book[description]"
      assert_select "textarea#book_source[name=?]", "book[source]"
      assert_select "textarea#book_mirror_links[name=?]", "book[mirror_links]"
    end
  end
end
