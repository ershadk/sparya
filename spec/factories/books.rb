# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :book do
    title "MyText"
    author "MyText"
    year "MyString"
    publisher "MyText"
    description "MyText"
    source "MyText"
    mirror_links "MyText"
  end
end
