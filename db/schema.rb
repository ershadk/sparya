# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130726145049) do

  create_table "books", :force => true do |t|
    t.text     "title"
    t.text     "author"
    t.string   "year"
    t.text     "publisher"
    t.text     "description"
    t.text     "source"
    t.text     "mirror_links"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "upload1_file_name"
    t.string   "upload1_content_type"
    t.integer  "upload1_file_size"
    t.datetime "upload1_updated_at"
    t.string   "upload2_file_name"
    t.string   "upload2_content_type"
    t.integer  "upload2_file_size"
    t.datetime "upload2_updated_at"
    t.text     "license"
  end

end
