class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.text :title
      t.text :author
      t.string :year
      t.text :publisher
      t.text :description
      t.text :source
      t.text :mirror_links

      t.timestamps
    end
  end
end
