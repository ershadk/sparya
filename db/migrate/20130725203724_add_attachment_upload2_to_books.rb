class AddAttachmentUpload2ToBooks < ActiveRecord::Migration
  def self.up
    change_table :books do |t|
      t.attachment :upload2
    end
  end

  def self.down
    drop_attached_file :books, :upload2
  end
end
