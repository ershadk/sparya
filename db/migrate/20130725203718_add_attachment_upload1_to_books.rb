class AddAttachmentUpload1ToBooks < ActiveRecord::Migration
  def self.up
    change_table :books do |t|
      t.attachment :upload1
    end
  end

  def self.down
    drop_attached_file :books, :upload1
  end
end
