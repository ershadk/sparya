class AddLicenseToBook < ActiveRecord::Migration
  def change
    add_column :books, :license, :text
  end
end
